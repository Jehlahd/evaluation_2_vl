###Droits Utilisateur (Non connecté)

*En tant que visiteur, je peux me créer un compte sur le site.

*En tant que visiteur, je peux me connecter au site.

*En tant que visiteur, j'ai accès à une procédure pour régénérer mon mot de passe en cas d'oubli.
---------------------------------------------------------------------------
###Droits Utilisateur (Connecté)

*En tant qu'utilisateur, je peux regarder les défis disponibles.

*En tant qu'utilisateur, je peux consulter chaque défi, je vois son intitulé, un résumé, des conseils laissés par d'autres utilisateurs ainsi que le nombre de personnes qui se sont lancées.

*En tant qu'utilisateur, je peux m'ajouter sur un défi.

*En tant qu'utilisateur, je peux ajouter un conseil sur un défi.

*En tant qu'utilisateur, je peux afficher mon profil avec l'ensemble de mes défis en cours et passés.

*En tant qu'utilisateur, je peux partager la liste de mes défis sur les réseaux sociaux.

*En tant qu'utilisateur, je peux me retirer d'un défi.
---------------------------------------------------------------------------
###Droits Admin

*En tant qu'administrateur, j'ai un CRUD pour les défis.

*En tant qu'administrateur, je peux consulter la liste des utilisateurs.

*En tant qu'administrateur, je peux bannir un utilisateur.