En l'état actuel, le projet ne répond pas à toutes les attentes du sujet de l'évaluation.

En effet, j'ai tenu à me consacrer à la création et à la manipulation d'une BDD via une interface web.

La sécurité, les connexions utilisateurs, l'hébergement en ligne et la PWA sont donc passés volontairement à la trappe, faute de temps de réflexion et de recherches.

J'ai tenté de joindre un fichier SQL de la BDD utilisé (eval_n_2.sql)

Ceci correspond au travail effectué durant les 2 jours d'évaluation.
