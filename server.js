const express = require('express');
const pug = require('pug');
const mysql = require('mysql');
const bodyparser = require('body-parser');
const bcryp = require('bcrypt-nodejs');
const port = 6789;
const app = express();


app.use(express.static("public"));
app.use(bodyparser.json());
var urlencodedparser = bodyparser.urlencoded({ extended: true});
app.use(urlencodedparser);

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'adminEval2',
    password: 'eval2#Simplon',
    database: 'eval_n_2'
});

var logged_user = "";
var message = "";

connection.connect();

app.set("view engine", "pug");

app.get("/", (req, res) => {
    res.render("accueil")
});

app.get("/testCreate", (req, res) => {
    res.render("testCreate")
});

app.post("/users/create", urlencodedparser, function(req, res) {
    var sql = "INSERT INTO users (password, mail, pseudo) VALUES (?, ?, ?);";
    var values = [req.body.password, req.body.mail, req.body.pseudo];
    sql = mysql.format(sql, values);
    connection.query(sql, function(error, results, fields) {
        if (!error) {
            res.redirect("/");
        }
        else {
            console.log(error);
            res.send(error);
        }
    })
});

app.get("/testUpdate", (req, res) => {
    res.render("testUpdate")
});

app.post("/users/update", urlencodedparser, function(req, res) {
    var sql = "UPDATE users SET mail = (?), password = (?) WHERE pseudo = (?);"
    var values = [req.body.mail, req.body.password, req.body.pseudo];
    sql = mysql.format(sql, values);
    connection.query(sql, function(error, results, fields) {
        if (!error) {
            res.redirect("/");
        }
        else {
            console.log(error);
            res.send(error);
        }
    })
})

app.get("/testDelete", (req, res) => {
    res.render("testDelete")
});

app.post("/users/delete", urlencodedparser, function (req, res) {
    var sql = "DELETE FROM users WHERE pseudo=(?);";
    var values = [req.body.pseudo];
    sql = mysql.format(sql, values);
    connection.query(sql, function(error, results, fields) {
        if (!error) {
            res.redirect("/");
        }
        else {
            console.log(error);
            res.send(error);
        }
    })
})

app.listen(port, function() {
    console.log("Le port n°" + port + " est actif.");
});